console.log("(Note: this site utilizes modern HTML/CSS/JS without backwards-compatible compilation. If you're experiencing issues, you might consider updating your browser to the latest Chrome or Firefox.)")

// universal helpers
//
const randomUpTo  = limit => Math.ceil(Math.random() * limit),
      wait        = ms    => new Promise(resolve => setTimeout(resolve, ms)),
      shuffle     = array => {
                      const output = array.slice(0);
                      for (let i = output.length - 1; i > 0; i--) {
                          const j = Math.floor(Math.random() * (i + 1));
                          [output[i], output[j]] = [output[j], output[i]];
                      }
                      return output;
                    };

// jQuery-free DOM helpers
//
const addClass    = (el, className)       => el.classList.add(className),
      removeClass = (el, className)       => el.classList.remove(className),
      changeStyle = (query, style, value) => select(query).forEach(el => el.style[style] = value),
      getParam    = param                 => (new URL(window.location).searchParams).get(param),
      fadeInEl    = async query           => addClass(select1(query), "fade-in"),
      clearEl     = async query           => select1(query).innerHTML = "",
      select1     = query                 => select(query)[0],
      select      = query                 => {
                                                const selected = document.querySelectorAll(query);
                                                return selected instanceof NodeList ? [...selected] : [selected]; // always returns true array
                                              };

// ethical free-server "warm-up" utility:
//
const pingSites = (...sites) => {
  sites.forEach(site => {
    const oReq = new XMLHttpRequest();
    oReq.open("GET", `https://${site}`);
    oReq.send();
  });
};

// text-animation logic
//
const removeOldCursor  = ()   => select1('.typed-cursor') ? select1('.typed-cursor').remove() : null
const humanifyLineEnds = line => `^${randomUpTo(1900)} ${line}^${randomUpTo(5000)}`; // adds randomized human-esque pre/post-type thought window for every entry.

const typeTitle = (titleString, speed) => new Promise(resolve => {
  removeOldCursor();
  new Typed(".cover-heading", {
    strings: [titleString],
    typeSpeed: speed,
    showCursor: true,
    cursorChar: "_",
    onComplete: resolve,
  });
  changeStyle('.typed-cursor', 'font-size', '30px'); // cursor size for header; it needs to be after, not before, the above block
});

const typeSubtitles = speed => {
  const thingsIAm = shuffle(['programmer','thinker','coder','developer',
  'problem solver','logician','"full^300 stack^300" developer',])
  .concat([
  'Web Application Programmer','SPA developer','polyglot',
  'node.js developer','linux^400 guy','react developer',
  'javasc^300.^100.^100.^100ript.^75.^75.^75er^200?^500 :/',
  'django developer','linguaphile','mithril developer',
  'python^400.^200.^200.^200.ista^200?^800 :/',
  'hmmm^200.^300.^400.^500.^600.','musician','TiNeSiFe',
  'writer','jiujiteiro','judoka','go^300 player',
  'esperantist','some guy','climber','austin native',
  'pretty^400 good^400 cook',
  ])
  .map(humanifyLineEnds);

  const thingsIDo = ["code^1000  |^600  judo^1000+bjj"];

  removeOldCursor(); // kill header's cursor

  new Typed(".lead", {
    strings: thingsIDo,
    typeSpeed: speed,
    backSpeed: 50,
    loop: false,
    shuffle: false,
    showCursor: true,
    cursorChar: "_",
  });
  changeStyle('.typed-cursor', "font-size", "24px");
};

// other custom effects stuff
//
const shineIcons = async (ms=600) => {
  // nice intro effect
  for (launcher of select1('.launchers').children) {
    addClass(launcher, 'full-opacity');
    await wait(ms);
    if (window.innerWidth > 500) removeClass(launcher, 'full-opacity');
  }
  return;
};

// user recency helpers
//
oldLVT = localStorage.LVT || null;
localStorage.LVT = new Date();

const lastVisit = {
  exists: !!oldLVT,
  get time() { return lastVisit.exists ? new Date(oldLVT) : null },
  get gap () {
    return getParam('hours') || lastVisit.fromNowIn.hours();
  },
  fromNowIn: {
    seconds: () => !lastVisit.exists ? 0 : new Date().getSeconds() - lastVisit.time.getSeconds(),
    hours:   () => lastVisit.fromNowIn.seconds() / 3600,
  },
};

// initialization modes
//
const fastInit = async () => {
  await fadeInEl('.masthead-nav');
  await fadeInEl('#jumbo');
  await typeTitle('kyle^1000 baker', 120);
  await fadeInEl('.launchers');
  await wait(400);
  await shineIcons(650);
  /*
  await typeSubtitles(100);
  */
};

const siteInit = async (speed=1000) => {
  // 2000 would be 2x slower (2 seconds per normal second), 500 would be 2x faster (.5 seconds per normal second).
  // Note: does not affect fade-in speed, but does effect pause after firing fade-ins as well as typing speed.

  await typeTitle('fadeIn^300(nav);^100', speed * .12);
  await wait(speed * 1.2);
  await clearEl('.cover-heading');
  await fadeInEl('.masthead-nav');
  // await wait(speed * 2.5);

  await typeTitle('fadeIn^300(coverImage);^100', speed * .12);
  await wait(speed * 1);
  await clearEl('.cover-heading');
  await fadeInEl('#jumbo');
  await wait(speed * 2.5);

  await typeTitle('shine^300(icons);^100', speed * .12);
  await wait(speed * 1);
  await clearEl('.cover-heading');
  await fadeInEl('.launchers');
  await wait(speed * 1);
  await shineIcons(speed * .65);

  await typeTitle('kyle^1000 baker', speed * .12);
  /*
  await wait(speed * 2);
  await typeSubtitles(100);
  */
};

// render site
//
if      (lastVisit.gap == 0) siteInit(900) && console.log("\nWelcome. Be sure to check the blog: http://code.kylebaker.io \n");
else if (lastVisit.gap < 48) fastInit()    && console.log("\nWelcome back. Good to see you again so soon.\n");
else                         siteInit(650) && console.log("\nWelcome back. It's been a while.\n");

pingSites('mindseal.kylebaker.io/warmup', 'zebrabowling.kylebaker.io/warmup', 'covid.kylebaker.io/warmup');
